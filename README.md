# Overview #

This project is a supplementary material for study in publication:

## Abstract ##

Modern software systems are not developed from
scratch – they rely heavily on the reuse of functionality provided
by libraries. Selecting the right libraries remains a challenging
task. What is more, libraries themselves often depend on other
libraries. Managing these transitive dependencies on libraries is
risky. In this paper, we describe the problems that can occur when
transitive dependencies are resolved automatically using examples
from real-world programs. We then present an empirical study
to assess the extent of the problem when the popular Maven tool
is used, and propose an approach based on static type checking
that can capture many of the problems described at build time.

*  Kamil Jezek, Jens Dietrich: **On the Use of Static Analysis to Safeguard Recursive Dependency Resolution**, proceedings of SEAA 2014, *IEEE* [[pdf](http://ieeexplore.ieee.org/document/6928807/?arnumber=6928807&tag=1)]


# Findings #

We have demonstrated that static analysis can be used to
check the compatibility between the users and the providers of
API and has the potential to prevent many of these problems.

We analysed a set of 1902 Maven modules, we detected 367 modules with du-
plicated dependencies. We detected significant numbers of
potential problems, including missing dependencies, references
to unused modules, incompatibilities between provided and
referenced classes and conflicting classes and also found
examples where these problems are a direct contribution of
duplicated modules on the classpath. In other words, it shows
that even correct components may break an application when
incorrectly composed. 

## Detail ## 

Project	 | 	Versions	 | 	Modules	 | 	Total	 | 	Missing | 	Redundant  | 	Incompatible 	 | 	Conflicting 
---------|-----------------------|---------------|---------------|--------------|------------------|---------------------|
ant		|	3	|	2	|	6	|	0	|	0	|	0	|	0	 
antlr		|	4	|	3	|	7	|	0	|	6	|	0	|	0	 
castor		|	1	|	3	|	3	|	0	|	3	|	0	|	0	 
cayenne		|	1	|	6	|	6	|	3	|	3	|	0	|	3	 
checkstyle	|	2	|	3	|	3	|	3	|	3	|	0	|	3	 
displaytag	|	1	|	2	|	2	|	2	|	2	|	1	|	2	 
gt2		|	2	|	19	|	23	|	4	|	12	|	1	|	4	 
hibernate	|	29	|	20	|	253	|	0	|	150	|	34	|	0	 
htmlunit	|	1	|	1	|	1	|	0	|	1	|	1	|	0	 
jasperreports	|	1	|	3	|	3	|	1	|	1	|	0	|	1	 
jboss		|	1	|	1	|	1	|	0	|	0	|	0	|	0	 
jruby		|	1	|	4	|	4	|	1	|	4	|	0	|	1	 
maven		|	1	|	2	|	2	|	0	|	1	|	0	|	0	 
nakedobjects	|	2	|	5	|	5	|	0	|	2	|	0	|	0	 
netbeans	|	1	|	1	|	1	|	0	|	1	|	0	|	0	 
pmd		|	1	|	1	|	1	|	0	|	0	|	0	|	0	 
springframework	|	1	|	8	|	8	|	1	|	1	|	0	|	1	 
struts		|	1	|	23	|	23	|	23	|	23	|	12	|	23	 
wct		|	1	|	1	|	1	|	0	|	0	|	0	|	0	 
Sums		|		|		|		|	38	|	213	|	49	|	38	 

	 

## Example ##


Detected duplications:
```
#!java

Dependency convergence error
+-o.a.struts:struts2-sitegraph-plugin:2.2.1
+-commons-io:commons-io:1.0
and
+-o.a.struts:struts2-sitegraph-plugin:2.2.1
+-org.apache.struts:struts2-core:2.2.1
+-commons-io:commons-io:1.3.2
```

Incompatibility caused by missing methods in the older commons-io library:

```
#!java

Filename: commons-io-1.0.jar
Class: org.apache.commons.io.IOUtils
Methods not found:
  copy(2), lineIterator(2), readLines(2), write(3), writeLines(4)
Imported by: org.apache.commons.io.FileUtils (xwork-core-2.2.1.jar)
```



