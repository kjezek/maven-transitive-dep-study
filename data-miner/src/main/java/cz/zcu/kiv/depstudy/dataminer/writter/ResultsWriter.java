package cz.zcu.kiv.depstudy.dataminer.writter;

import cz.zcu.kiv.depstudy.dataminer.data.CCP3Errors;
import cz.zcu.kiv.depstudy.dataminer.data.ProjectItem;
import cz.zcu.kiv.depstudy.dataminer.results.Results;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ResultsWriter {


    public static void writeProjectMatching(
            Map<ProjectItem, ProjectItem> projectsMap,
            String file) throws IOException {

        List<String> lines = new LinkedList<String>();

        lines.add("Project,Version,Module,Project,Version,Module," +
                "Incompatible,Unused,Conflicting,Missing," +
                "Matched Incompat," +
                "Number Duplicated," +
                "Number Incompatible");

        int numberOfProjectsWithMissingImports = 0;
        int numberOfProjectsWithIncompatibilities = 0;
        int numberOfProjectsWithUnusedImports = 0;
        int numberOfProjectsWithConflictingNames = 0;

        int numberOfProjectWithEnforcerDuplicities = 0;
        int numberOfProjectsWithCcp3Problems = 0;
        int numberOfMatchedProjects = 0;

        for (ProjectItem project1 : projectsMap.keySet()) {

            ProjectItem project2 = projectsMap.get(project1);

            CCP3Errors errors = new CCP3Errors();

            if (StringUtils.trimToNull(project2.getFile()) != null) {
                errors = Results.analyseIncompatibilities(project2.getFile());
            }

            if (errors.getMissingCount() > 0) {
                numberOfProjectsWithMissingImports++;
            }

            if (errors.isIncompatibilities()) {
                numberOfProjectsWithIncompatibilities++;
            }

            if (errors.isUnusedImports()) {
                numberOfProjectsWithUnusedImports++;
            }
            if (errors.isConflictingNames()) {
                numberOfProjectsWithConflictingNames++;
            }

            Set<String> enforcerClashes = Results.analyseEnforcerResults(project1.getFile());
            Set<String> ccp3incompat = errors.getIncompatible();

            if (enforcerClashes.size() > 0) {
                numberOfProjectWithEnforcerDuplicities++;
            }

            if (ccp3incompat.size() > 0) {
                numberOfProjectsWithCcp3Problems++;
            }

            Set<String> matchedIncompatibilityProblems = new HashSet<>(enforcerClashes);
            matchedIncompatibilityProblems.retainAll(ccp3incompat);

            if (matchedIncompatibilityProblems.size() > 0) {
                numberOfMatchedProjects++;
            }

            lines.add(project1.getProject() + ","
                    + project1.getVersion() + ","
                    + project1.getSubProject() + ","
                    + project2.getProject() + ","
                    + project2.getVersion() + ","
                    + project2.getSubProject() + ","
                    + toStr(errors.isIncompatibilities()) + ","
                    + toStr(errors.isUnusedImports()) + ","
                    + toStr(errors.isConflictingNames()) + ","
                    + toStr(errors.getMissingCount() > 0) + ","
                    + matchedIncompatibilityProblems.size() + ","
                    + enforcerClashes.size() + ","
                    + ccp3incompat.size() + ","
                    + enforcerClashes.toString().replace(',',' ') + ","
                    + ccp3incompat.toString().replace(',',' ') + ","
            );

        }

        lines.add("Counts,,,,,," + numberOfProjectsWithIncompatibilities + "," +
                numberOfProjectsWithUnusedImports + "," +
                numberOfProjectsWithConflictingNames + "," +
                numberOfProjectsWithMissingImports + "," +
                numberOfMatchedProjects + "," +
                numberOfProjectWithEnforcerDuplicities + "," +
                numberOfProjectsWithCcp3Problems
                );

        FileUtils.writeLines(new File(file), lines);

    }

    public static void writeProjectCounts(
            Map<ProjectItem, ProjectItem> projectsMap,
            String file) throws IOException {

        List<ProjectItem> projects = new ArrayList<>(projectsMap.values());


        Set<String> projectNames = new HashSet<>();
        Map<String, Set<String>> versions = new HashMap<>();
        Map<String, Set<String>> modules = new HashMap<>();
        Map<String, Set<String>> modulesVersion = new HashMap<>();
        Map<String,Integer> incompatible = new HashMap<>();
        Map<String,Integer> unused = new HashMap<>();
        Map<String,Integer> conflicting = new HashMap<>();
        Map<String,Integer> missing = new HashMap<>();

        for (ProjectItem item : projects) {

            if (StringUtils.trimToNull(item.getFile()) == null) {
                continue;
            }

            CCP3Errors errors = Results.analyseIncompatibilities(item.getFile());

            projectNames.add(item.getProject());

            add(versions, item.getProject(), item.getVersion());
            add(modules, item.getProject(), item.getProject() + "-" + item.getSubProject());
            add(modulesVersion, item.getProject(), item.getVersion() + "-" + item.getSubProject());


            if (errors.getMissingCount() > 0) {
                incValue(missing, item.getProject());
            }

            if (errors.isIncompatibilities()) {
                incValue(incompatible, item.getProject());
            }

            if (errors.isUnusedImports()) {
                incValue(unused, item.getProject());
            }

            if (errors.isConflictingNames()) {
                incValue(conflicting, item.getProject());
            }

        }

        List<String> projectNamesSorted = new ArrayList<>(projectNames);
        Collections.sort(projectNamesSorted);

        List<String> lines = new LinkedList<>();
        lines.add("Project,Number of Versions,Number of Modules,Total,Incompatible,Unused,Conflicting,Missing,");

        for (String projectName : projectNamesSorted) {

            lines.add(projectName + "," +
                    versions.get(projectName).size() + "," +
                    modules.get(projectName).size() + "," +
                    modulesVersion.get(projectName).size() + "," +
                    nn(incompatible.get(projectName)) + "," +
                    nn(unused.get(projectName)) + "," +
                    nn(conflicting.get(projectName)) + "," +
                    nn(missing.get(projectName)) + ",");
        }


        FileUtils.writeLines(new File(file), lines);
    }

    private static String toStr(boolean input) {
        if (input) {
            return "x";
        }

        return  "";
    }

    private static String nn(Integer input) {
        if (input == null) {
            return "0";
        }

        return input.toString();
    }

    public static <T> void  add(Map<String,Set<T>> map, String key, T item) {

        Set<T> value = map.get(key);
        if (value == null) {
            value = new HashSet<>();
            map.put(key, value);
        }

        value.add(item);
    }

    private static void incValue(Map<String, Integer> map, String key) {

        Integer value = map.get(key);
        if (value == null) {
            value = 0;
        }
        value++;

        map.put(key, value);
    }
}
