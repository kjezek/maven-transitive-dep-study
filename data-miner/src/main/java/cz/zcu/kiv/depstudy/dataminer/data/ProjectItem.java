package cz.zcu.kiv.depstudy.dataminer.data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ProjectItem {

    private String project;
    private String version;
    private String subProject;
    private String file;

    public ProjectItem(String project, String version, String subProject, String file) {
        this.project = project;
        this.version = version;
        this.subProject = subProject;
        this.file = file;
    }

    public String getProject() {
        return project;
    }

    public String getVersion() {
        return version;
    }

    public String getSubProject() {
        return subProject;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectItem that = (ProjectItem) o;

        if (project != null ? !project.equals(that.project) : that.project != null) return false;
        if (subProject != null ? !subProject.equals(that.subProject) : that.subProject != null) return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = project != null ? project.hashCode() : 0;
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (subProject != null ? subProject.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProjectItem{" +
                "project='" + project + '\'' +
                ", version='" + version + '\'' +
                ", subProject='" + subProject + '\'' +
                '}';
    }
}
