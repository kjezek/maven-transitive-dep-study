package cz.zcu.kiv.depstudy.dataminer.readers;

import cz.zcu.kiv.depstudy.dataminer.data.ProjectItem;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class DirectoryReader {

    private String dir;

    public DirectoryReader(String dir) {
        this.dir = dir;
    }

    public List<ProjectItem> readProjects() {

        List<ProjectItem> items = new LinkedList<ProjectItem>();
        for (String file: new File(dir).list()) {

            // bogus in data
            if (file.equals(".txt")) {
                continue;
            }

            String[] parsed = file.split(".txt")[0].split("_");

            ProjectItem item = new ProjectItem(
                    parsed[0],
                    parsed[1],
                    parsed[parsed.length-1],
                    dir + "/" + file);

            items.add(item);
        }

        return items;
    }
}
