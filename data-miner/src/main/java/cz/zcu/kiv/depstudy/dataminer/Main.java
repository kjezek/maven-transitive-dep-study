package cz.zcu.kiv.depstudy.dataminer;

import cz.zcu.kiv.depstudy.dataminer.data.CCP3Errors;
import cz.zcu.kiv.depstudy.dataminer.data.ProjectItem;
import cz.zcu.kiv.depstudy.dataminer.readers.DirectoryReader;
import cz.zcu.kiv.depstudy.dataminer.results.Results;
import cz.zcu.kiv.depstudy.dataminer.writter.ResultsWriter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class Main {

    public static void main(String[] args) throws IOException {

        List<ProjectItem> projectsByEnforcer = new DirectoryReader("/home/kamilos/projects/maven-transitive-dep-study/problem-quantif-study/enforcer-results")
                .readProjects();

        List<ProjectItem> projectsByCcp3 = new DirectoryReader("/home/kamilos/projects/maven-transitive-dep-study/problem-quantif-study/ccp3-results")
                .readProjects();

        Map<ProjectItem, ProjectItem> matchedProjects = Results.mapProjects(projectsByEnforcer, projectsByCcp3);

        ResultsWriter.writeProjectMatching(
                matchedProjects,
                "/home/kamilos/projects/maven-transitive-dep-study/problem-quantif-study/results-match.csv");

        ResultsWriter.writeProjectCounts(matchedProjects,
                "/home/kamilos/projects/maven-transitive-dep-study/problem-quantif-study/projects-counts.csv");
    }

}
