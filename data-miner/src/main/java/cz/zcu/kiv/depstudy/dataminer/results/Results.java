package cz.zcu.kiv.depstudy.dataminer.results;

import cz.zcu.kiv.depstudy.dataminer.data.CCP3Errors;
import cz.zcu.kiv.depstudy.dataminer.data.ProjectItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import sun.util.logging.resources.logging;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class Results {


    public static Map<ProjectItem, ProjectItem> mapProjects(List<ProjectItem> projects1, List<ProjectItem> projects2) {

        Map<ProjectItem, ProjectItem> map = new HashMap<ProjectItem, ProjectItem>();

        for (ProjectItem project1 : projects1) {

            map.put(project1, new ProjectItem("", "", "", ""));  // empty by default
            for (ProjectItem project2 : projects2) {

                // map equal projects
                if (project1.equals(project2)) {
                    // there should be one to one or one to null mapping
                    map.put(project1, project2);
                    break;
                }
            }
        }

        return map;
    }

    /**
     * Find CCP3 type erros in a file
     * @param file
     * @return
     */
    public static CCP3Errors analyseIncompatibilities(String file) throws IOException {

        CCP3Errors errors = new CCP3Errors();

        int curentSection = 0;
        LineIterator it = FileUtils.lineIterator(new File(file));
        while (it.hasNext()) {
            String line = it.nextLine();

            if (line.contains("Unused imports found")) {
                errors.setUnusedImports(true);
                curentSection = 1;
            }

            if (line.contains("Conficting class names found")) {
                errors.setConflictingNames(true);
                curentSection = 2;
            }

            if (line.contains("Incompatibilities found")) {
                errors.setIncompatibilities(true);
                curentSection = 3;
            }

            // missing imports section
            if (curentSection == 2
                    && line.contains(".jar")) {

                String item = line.substring("[ERROR]".length()).split("\\.jar")[0].trim();
                errors.getMissingFrom().add(item);
                errors.incMissingCount();
            }

            // incompatibilities section
            if (curentSection == 3
                    && line.contains("Filename:")) {

                // the line is for instance:
                // [ERROR] Filename: log4j-1.2.9.jar

                String item = line.split("Filename: ")[1].split("\\.jar")[0];
                errors.getIncompatible().add(item);
            }
        }

        return errors;
    }

    public static Set<String> analyseEnforcerResults(String file) throws IOException {


        // data are structured as follows. We need to collect leafs of this tree
//        +-nz.govt.natlib.ndha.wctdpsdepositor:WCTSubmitToRosetta:1.0-SNAPSHOT
//             +-commons-httpclient:commons-httpclient:3.1
//                +-commons-logging:commons-logging:1.0.4

        Set<String> items = new HashSet<>();

        LineIterator it = FileUtils.lineIterator(new File(file));
        while (it.hasNext()) {
            String line = it.nextLine();

            if (line.contains("+-")) {

                String leaf = null;
                while (line.contains("+-")) {
                    leaf = line;
                    line = it.nextLine();
                }

                String[] components = leaf.split("\\+\\-")[1].split(":");
                String item = components[1] + "-"  + components[2];
                items.add(item);
            }
        }

        return items;
    }
}
