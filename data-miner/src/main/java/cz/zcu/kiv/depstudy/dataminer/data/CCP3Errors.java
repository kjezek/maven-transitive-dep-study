package cz.zcu.kiv.depstudy.dataminer.data;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class CCP3Errors {

    private boolean incompatibilities = false;
    private boolean unusedImports = false;
    private boolean conflictingNames = false;
    private boolean missingImports = false;

    private Set<String> incompatible = new HashSet<>();
    private Set<String> missingFrom = new HashSet<>();
    private int missingCount = 0;

    public void setIncompatibilities(boolean incompatibilities) {
        this.incompatibilities = incompatibilities;
    }

    public void setUnusedImports(boolean unusedImports) {
        this.unusedImports = unusedImports;
    }

    public void setConflictingNames(boolean conflictingNames) {
        this.conflictingNames = conflictingNames;
    }

    public void setMissingImports(boolean missingImports) {
        this.missingImports = missingImports;
    }

    public boolean isIncompatibilities() {
        return incompatibilities;
    }

    public boolean isUnusedImports() {
        return unusedImports;
    }

    public boolean isConflictingNames() {
        return conflictingNames;
    }

    public boolean isMissingImports() {
        return missingImports;
    }

    public Set<String> getIncompatible() {
        return incompatible;
    }

    public Set<String> getMissingFrom() {
        return missingFrom;
    }

    public void incMissingCount() {
        missingCount++;
    }

    public int getMissingCount() {
        return missingCount;
    }
}
