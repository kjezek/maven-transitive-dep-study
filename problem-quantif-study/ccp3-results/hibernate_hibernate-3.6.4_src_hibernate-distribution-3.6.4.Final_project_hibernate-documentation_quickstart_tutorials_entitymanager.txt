[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate JPA Tutorial 3.6.4.Final
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- compatibility-checking-plugin:0.0.1:check (default-cli) @ hibernate-tutorial-entitymanager ---
[INFO] Processing project Hibernate JPA Tutorial
[INFO] Adding artifact hibernate-entitymanager for processing
[INFO] Adding artifact cglib for processing
[INFO] Adding artifact asm for processing
[INFO] Adding artifact hibernate-jpa-2.0-api for processing
[INFO] Adding artifact slf4j-api for processing
[INFO] Adding artifact hibernate-core for processing
[INFO] Adding artifact antlr for processing
[INFO] Adding artifact commons-collections for processing
[INFO] Adding artifact dom4j for processing
[INFO] Adding artifact hibernate-commons-annotations for processing
[INFO] Adding artifact jta for processing
[INFO] Adding artifact slf4j-simple for processing
[INFO] Adding artifact javassist for processing
[INFO] Adding artifact junit for processing
[INFO] Adding artifact h2 for processing
[ERROR] Cannot find file for artifact: org.hibernate.tutorials:hibernate-tutorial-entitymanager:jar:3.6.4.Final
[INFO] Adding jars from JAVA_HOME
[INFO] Adding /opt/jdk1.7/jre/lib/rt.jar
[INFO] Processing artifacts compatibility check. This may take some time, please be patient.
[INFO] Loading classes into memory from byte code
[INFO] Analyzing compatibility problems
[WARNING] --------------------------
[WARNING] -- Unused imports found --
[WARNING] --------------------------
[WARNING] 	commons-collections-3.1.jar
[WARNING] 	hibernate-core-3.6.4.Final.jar
[WARNING] 	slf4j-api-1.6.1.jar
[WARNING] 	hibernate-commons-annotations-3.2.0.Final.jar
[WARNING] 	javassist-3.12.0.GA.jar
[WARNING] 	antlr-2.7.6.jar
[WARNING] 	dom4j-1.6.1.jar
[WARNING] 	h2-1.2.140.jar
[WARNING] 	hibernate-jpa-2.0-api-1.0.0.Final.jar
[WARNING] 	hibernate-entitymanager-3.6.4.Final.jar
[WARNING] 	junit-4.8.1.jar
[WARNING] 	cglib-2.2.jar
[WARNING] 	asm-3.1.jar
[WARNING] 	jta-1.1.jar
[WARNING] 	slf4j-simple-1.6.1.jar
[WARNING] 
[INFO] -------------------------------------
[INFO] -- No conficting class names found --
[INFO] -------------------------------------
[ERROR] ---------------------------
[ERROR] -- Missing imports found --
[ERROR] ---------------------------
[ERROR] Missing classes: 
[ERROR] 	com.sun.msv.datatype.xsd.TypeIncubator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	org.gjt.xpp.XmlEndTag
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	org.apache.lucene.document.Field$Store
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	com.sun.jdi.connect.IllegalConnectorArgumentsException
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.apache.tools.ant.DirectoryScanner
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.apache.lucene.search.IndexSearcher
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 	javax.security.jacc.EJBMethodPermission
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPreInsertEventListener
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPreLoadEventListener
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPreDeleteEventListener
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPreUpdateEventListener
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	com.sun.jdi.connect.Connector
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.xmlpull.v1.XmlPullParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	org.apache.lucene.document.Fieldable
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	org.apache.lucene.search.Searcher
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 	org.xmlpull.v1.XmlPullParserException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	org.apache.lucene.document.Document
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	org.jaxen.Context
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.saxpath.SAXPathException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.jdi.event.MethodEntryEvent
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	com.sun.msv.datatype.SerializationContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	org.jaxen.ContextSupport
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.XPathFunctionContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.gjt.xpp.XmlPullParserException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpp.ProxyXmlStartTag
[ERROR] 	org.jaxen.dom4j.Dom4jXPath
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 	javax.validation.constraints.Max
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.Path
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.HibernateTraversableResolver
[ERROR] 	org.xmlpull.v1.XmlPullParserFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	javax.security.jacc.PolicyConfigurationFactory
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	org.apache.tools.ant.BuildException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.apache.tools.ant.Project
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.jaxen.pattern.PatternParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.apache.lucene.document.Field
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	com.sun.jdi.VirtualMachine
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.osgi.framework.BundleContext
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.util.DbDriverActivator
[ERROR] 	org.apache.lucene.search.Hits
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 	org.apache.tools.ant.Task
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 	javax.validation.constraints.Digits
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	org.jaxen.dom4j.DocumentNavigator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	javax.validation.metadata.BeanDescriptor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.metadata.PropertyDescriptor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	org.gjt.xpp.XmlPullParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	javax.validation.constraints.Size
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.groups.Default
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.GroupsPerOperation
[ERROR] 	javax.servlet.ServletContextListener
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.server.web.DbStarter
[ERROR] 	javax.validation.ValidatorContext
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 	javax.validation.Validator
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	org.apache.lucene.index.Term
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	org.relaxng.datatype.DatatypeException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	javax.servlet.http.HttpServletRequest
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.server.web.WebServlet
[ERROR] 	com.sun.jdi.VirtualMachineManager
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.msv.datatype.xsd.DatatypeFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	org.apache.lucene.analysis.standard.StandardAnalyzer
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 	org.apache.lucene.search.Query
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 	com.sun.jdi.request.EventRequestManager
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	javax.servlet.ServletConfig
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.server.web.WebServlet
[ERROR] 	com.sun.jdi.connect.Connector$Argument
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.jdi.event.Event
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	org.jaxen.pattern.Pattern
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.msv.datatype.xsd.XSDatatypeImpl
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	javax.validation.Validation
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.ConstraintViolationException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 	org.apache.lucene.queryParser.QueryParser
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 	org.apache.lucene.document.DateTools$Resolution
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	javax.servlet.ServletContextEvent
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.server.web.DbStarter
[ERROR] 	org.apache.lucene.index.IndexReader
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 	javax.security.jacc.PolicyConfiguration
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	com.sun.msv.datatype.DatabindableDatatype
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	com.sun.jdi.event.EventSet
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	javax.validation.ConstraintViolation
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 	org.apache.lucene.document.DateTools
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	com.sun.jdi.Bootstrap
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.jdi.event.EventIterator
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	org.apache.lucene.index.IndexModifier
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	javax.security.jacc.PolicyContextException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$2
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1$1
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	javax.servlet.http.HttpServlet
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.server.web.WebServlet
[ERROR] 	org.objectweb.asm.util.TraceClassVisitor
[ERROR] 		Imported by: 
[ERROR] 		 cglib-2.2.jar:net.sf.cglib.core.DebuggingClassWriter$1
[ERROR] 	com.sun.jdi.request.MethodEntryRequest
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.relaxng.datatype.ValidationContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	org.apache.tools.ant.types.FileSet
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.gjt.xpp.XmlStartTag
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpp.ProxyXmlStartTag
[ERROR] 	org.osgi.framework.BundleActivator
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.util.DbDriverActivator
[ERROR] 	org.jaxen.SimpleVariableContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.apache.lucene.document.Field$Index
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene$FullTextTrigger
[ERROR] 	javax.validation.constraints.Min
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	com.sun.jdi.ReferenceType
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.jaxen.FunctionContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.XPath
[ERROR] 	org.h2.test.TestBase
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.java.Test
[ERROR] 	com.sun.jdi.request.EventRequest
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	javax.security.jacc.PolicyContext
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$2
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1$1
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.secure.JACCPermissions$SetContextID
[ERROR] 	javax.servlet.ServletContext
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.server.web.DbStarter
[ERROR] 	org.jaxen.JaxenException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	javax.servlet.ServletOutputStream
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.server.web.WebServlet
[ERROR] 	javax.servlet.http.HttpServletResponse
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.server.web.WebServlet
[ERROR] 	org.gjt.xpp.XmlPullParserFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	javax.validation.ValidatorFactory
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	org.jaxen.NamespaceContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultNamespaceContext
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	javax.validation.Path$Node
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.HibernateTraversableResolver
[ERROR] 	org.jaxen.SimpleNamespaceContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.Navigator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	javax.validation.constraints.NotNull
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	com.sun.jdi.connect.AttachingConnector
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.jaxen.VariableContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.DocumentHelper
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.DocumentFactory
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.XPath
[ERROR] 	javax.validation.metadata.ConstraintDescriptor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.TraversableResolver
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.cfg.beanvalidation.HibernateTraversableResolver
[ERROR] 	com.sun.msv.datatype.xsd.XSDatatype
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.NamedTypeResolver
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElementFactory
[ERROR] 	org.apache.tools.ant.taskdefs.MatchingTask
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.4.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 	org.jaxen.XPath
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.rule.Stylesheet
[ERROR] 	org.apache.lucene.analysis.Analyzer
[ERROR] 		Imported by: 
[ERROR] 		 h2-1.2.140.jar:org.h2.fulltext.FullTextLucene
[ERROR] 	com.sun.jdi.event.EventQueue
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 
[INFO] --------------------------------
[INFO] -- All bundles are compatible --
[INFO] --------------------------------
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1:29.945s
[INFO] Finished at: Tue Feb 04 15:12:28 CET 2014
[INFO] Final Memory: 494M/1160M
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal cz.slezacek.ccp3:compatibility-checking-plugin:0.0.1:check (default-cli) on project hibernate-tutorial-entitymanager: Compatibility problems were found. Details above. -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException
