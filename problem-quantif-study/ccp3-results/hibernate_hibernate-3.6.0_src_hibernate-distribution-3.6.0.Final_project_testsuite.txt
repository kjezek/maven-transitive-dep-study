[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for org.hibernate:hibernate-testsuite:jar:3.6.0.Final
[WARNING] The expression ${groupId} is deprecated. Please use ${project.groupId} instead.
[WARNING] The expression ${version} is deprecated. Please use ${project.version} instead.
[WARNING] The expression ${groupId} is deprecated. Please use ${project.groupId} instead.
[WARNING] The expression ${version} is deprecated. Please use ${project.version} instead.
[WARNING] The expression ${inceptionYear} is deprecated. Please use ${project.inceptionYear} instead.
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate Testsuite 3.6.0.Final
[INFO] ------------------------------------------------------------------------
[WARNING] The POM for commons-logging:commons-logging:jar:99.0-does-not-exist is missing, no dependency information available
[WARNING] The POM for commons-logging:commons-logging-api:jar:99.0-does-not-exist is missing, no dependency information available
[INFO] 
[INFO] --- compatibility-checking-plugin:0.0.1:check (default-cli) @ hibernate-testsuite ---
[INFO] Processing project Hibernate Testsuite
[INFO] Adding artifact hibernate-core for processing
[INFO] Adding artifact antlr for processing
[INFO] Adding artifact commons-collections for processing
[INFO] Adding artifact dom4j for processing
[INFO] Adding artifact hibernate-commons-annotations for processing
[INFO] Adding artifact hibernate-jpa-2.0-api for processing
[INFO] Adding artifact jta for processing
[INFO] Adding artifact hibernate-testing for processing
[INFO] Adding artifact javassist for processing
[INFO] Adding artifact cglib for processing
[INFO] Adding artifact asm for processing
[INFO] Adding artifact jaxen for processing
[INFO] Adding artifact jdom for processing
[INFO] Adding artifact slf4j-api for processing
[ERROR] Cannot find file for artifact: org.hibernate:hibernate-testsuite:jar:3.6.0.Final
[INFO] Adding jars from JAVA_HOME
[INFO] Adding /opt/jdk1.7/jre/lib/rt.jar
[INFO] Processing artifacts compatibility check. This may take some time, please be patient.
[INFO] Loading classes into memory from byte code
[INFO] Analyzing compatibility problems
[WARNING] --------------------------
[WARNING] -- Unused imports found --
[WARNING] --------------------------
[WARNING] 	commons-collections-3.1.jar
[WARNING] 	hibernate-core-3.6.0.Final.jar
[WARNING] 	slf4j-api-1.6.1.jar
[WARNING] 	hibernate-commons-annotations-3.2.0.Final.jar
[WARNING] 	javassist-3.12.0.GA.jar
[WARNING] 	antlr-2.7.6.jar
[WARNING] 	dom4j-1.6.1.jar
[WARNING] 	hibernate-jpa-2.0-api-1.0.0.Final.jar
[WARNING] 	jaxen-1.1.jar
[WARNING] 	hibernate-testing-3.6.0.Final.jar
[WARNING] 	jdom-1.0.jar
[WARNING] 	cglib-2.2.jar
[WARNING] 	asm-3.1.jar
[WARNING] 	jta-1.1.jar
[WARNING] 
[INFO] -------------------------------------
[INFO] -- No conficting class names found --
[INFO] -------------------------------------
[ERROR] ---------------------------
[ERROR] -- Missing imports found --
[ERROR] ---------------------------
[ERROR] Missing classes: 
[ERROR] 	com.sun.msv.datatype.xsd.TypeIncubator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	org.slf4j.impl.StaticMarkerBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.6.1.jar:org.slf4j.MarkerFactory
[ERROR] 	nu.xom.NodeFactory
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 	org.gjt.xpp.XmlEndTag
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	nu.xom.ParentNode
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator$2
[ERROR] 	com.sun.jdi.request.EventRequestManager
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.jdi.connect.Connector$Argument
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	junit.framework.AssertionFailedError
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.UnitTestCase
[ERROR] 	com.sun.jdi.connect.IllegalConnectorArgumentsException
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.jdi.event.Event
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	org.apache.tools.ant.DirectoryScanner
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	com.sun.msv.datatype.xsd.XSDatatypeImpl
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	javax.security.jacc.EJBMethodPermission
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPreInsertEventListener
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPreLoadEventListener
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPreDeleteEventListener
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPreUpdateEventListener
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	com.sun.jdi.connect.Connector
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	javax.validation.Validation
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.ConstraintViolationException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 	org.xmlpull.v1.XmlPullParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	junit.framework.TestCase
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.UnitTestCase
[ERROR] 	org.xmlpull.v1.XmlPullParserException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	org.slf4j.impl.StaticLoggerBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.6.1.jar:org.slf4j.LoggerFactory
[ERROR] 	com.sun.jdi.event.MethodEntryEvent
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	com.sun.msv.datatype.SerializationContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	nu.xom.Element
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator$1
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator$XPathNamespace
[ERROR] 	javax.security.jacc.PolicyConfiguration
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	com.sun.msv.datatype.DatabindableDatatype
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	com.sun.jdi.event.EventSet
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	javax.validation.ConstraintViolation
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 	org.gjt.xpp.XmlPullParserException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpp.ProxyXmlStartTag
[ERROR] 	javax.validation.constraints.Max
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.Path
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.HibernateTraversableResolver
[ERROR] 	com.sun.jdi.Bootstrap
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.xmlpull.v1.XmlPullParserFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	com.sun.jdi.event.EventIterator
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	javax.security.jacc.PolicyContextException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$2
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1$1
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	JDOMAbout
[ERROR] 		Imported by: 
[ERROR] 		 jdom-1.0.jar:.JDOMAbout
[ERROR] 	javax.security.jacc.PolicyConfigurationFactory
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	org.slf4j.impl.StaticMDCBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.6.1.jar:org.slf4j.MDC
[ERROR] 	org.objectweb.asm.util.TraceClassVisitor
[ERROR] 		Imported by: 
[ERROR] 		 cglib-2.2.jar:net.sf.cglib.core.DebuggingClassWriter$1
[ERROR] 	com.sun.jdi.request.MethodEntryRequest
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.apache.tools.ant.Project
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.apache.tools.ant.BuildException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.gjt.xpp.XmlStartTag
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpp.ProxyXmlStartTag
[ERROR] 	org.relaxng.datatype.ValidationContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	org.apache.tools.ant.types.FileSet
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	com.sun.jdi.VirtualMachine
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	javax.validation.constraints.Min
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	nu.xom.Comment
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 	com.sun.jdi.ReferenceType
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.apache.tools.ant.Task
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 	javax.validation.constraints.Digits
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	com.sun.jdi.request.EventRequest
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	javax.security.jacc.PolicyContext
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$2
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1$1
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.secure.JACCPermissions$SetContextID
[ERROR] 	JDOMAbout$Author
[ERROR] 		Imported by: 
[ERROR] 		 jdom-1.0.jar:.JDOMAbout
[ERROR] 	javax.validation.metadata.BeanDescriptor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.metadata.PropertyDescriptor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	org.gjt.xpp.XmlPullParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	nu.xom.Attribute
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator$1
[ERROR] 	org.gjt.xpp.XmlPullParserFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	javax.validation.constraints.Size
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.ValidatorFactory
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	junit.framework.Test
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.TestSuiteVisitor
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.TestSuiteVisitor$Handler
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.UnitTestCase
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.functional.FunctionalTestClassTestSuite
[ERROR] 	javax.validation.groups.Default
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.GroupsPerOperation
[ERROR] 	nu.xom.Builder
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 	nu.xom.ProcessingInstruction
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 	javax.validation.Path$Node
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.HibernateTraversableResolver
[ERROR] 	javax.validation.ValidatorContext
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 	javax.validation.constraints.NotNull
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	com.sun.jdi.connect.AttachingConnector
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	javax.validation.Validator
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.metadata.ConstraintDescriptor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.TypeSafeActivator
[ERROR] 	javax.validation.TraversableResolver
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.BeanValidationEventListener
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.cfg.beanvalidation.HibernateTraversableResolver
[ERROR] 	junit.framework.TestResult
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.functional.FunctionalTestClassTestSuite
[ERROR] 	org.relaxng.datatype.DatatypeException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	nu.xom.Text
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 	com.sun.msv.datatype.xsd.XSDatatype
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.NamedTypeResolver
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElementFactory
[ERROR] 	nu.xom.Document
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 	org.apache.tools.ant.taskdefs.MatchingTask
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.6.0.Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 	JDOMAbout$Info
[ERROR] 		Imported by: 
[ERROR] 		 jdom-1.0.jar:.JDOMAbout
[ERROR] 	nu.xom.Node
[ERROR] 		Imported by: 
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator
[ERROR] 		 jaxen-1.1.jar:org.jaxen.xom.DocumentNavigator$2
[ERROR] 	junit.framework.TestSuite
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.TestSuiteVisitor
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.TestSuiteVisitor$Handler
[ERROR] 		 hibernate-testing-3.6.0.Final.jar:org.hibernate.testing.junit.functional.FunctionalTestClassTestSuite
[ERROR] 	com.sun.jdi.VirtualMachineManager
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.msv.datatype.xsd.DatatypeFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	com.sun.jdi.event.EventQueue
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 
[INFO] --------------------------------
[INFO] -- All bundles are compatible --
[INFO] --------------------------------
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1:33.549s
[INFO] Finished at: Tue Feb 04 14:13:41 CET 2014
[INFO] Final Memory: 476M/1144M
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal cz.slezacek.ccp3:compatibility-checking-plugin:0.0.1:check (default-cli) on project hibernate-testsuite: Compatibility problems were found. Details above. -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException
