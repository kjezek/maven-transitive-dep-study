[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Checkstyle 4.3
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- compatibility-checking-plugin:0.0.1:check (default-cli) @ checkstyle ---
[INFO] Processing project Checkstyle
[INFO] Adding artifact antlr for processing
[INFO] Adding artifact commons-beanutils-core for processing
[INFO] Adding artifact commons-cli for processing
[INFO] Adding artifact commons-lang for processing
[INFO] Adding artifact junit for processing
[INFO] Adding artifact commons-collections for processing
[INFO] Adding artifact commons-logging for processing
[ERROR] Cannot find file for artifact: checkstyle:checkstyle:jar:4.3
[INFO] Adding jars from JAVA_HOME
[INFO] Adding /opt/jdk1.7/jre/lib/rt.jar
[INFO] Processing artifacts compatibility check. This may take some time, please be patient.
[INFO] Loading classes into memory from byte code
[INFO] Analyzing compatibility problems
[WARNING] --------------------------
[WARNING] -- Unused imports found --
[WARNING] --------------------------
[WARNING] 	commons-beanutils-core-1.7.0.jar
[WARNING] 	commons-collections-2.1.jar
[WARNING] 	antlr-2.7.6.jar
[WARNING] 	junit-3.7.jar
[WARNING] 	commons-lang-1.0.jar
[WARNING] 	commons-cli-1.0.jar
[WARNING] 	commons-logging-1.0.3.jar
[WARNING] 
[ERROR] ----------------------------------
[ERROR] -- Conficting class names found --
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.FastHashMap$CollectionView$CollectionViewIterator
[ERROR] Found in: 
[ERROR] 	commons-collections-2.1.jar
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.ArrayStack
[ERROR] Found in: 
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] 	commons-collections-2.1.jar
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.FastHashMap$CollectionView
[ERROR] Found in: 
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] 	commons-collections-2.1.jar
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.FastHashMap$KeySet
[ERROR] Found in: 
[ERROR] 	commons-collections-2.1.jar
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.BufferUnderflowException
[ERROR] Found in: 
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] 	commons-collections-2.1.jar
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.Buffer
[ERROR] Found in: 
[ERROR] 	commons-collections-2.1.jar
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.FastHashMap$EntrySet
[ERROR] Found in: 
[ERROR] 	commons-collections-2.1.jar
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.FastHashMap
[ERROR] Found in: 
[ERROR] 	commons-collections-2.1.jar
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] ----------------------------------
[ERROR] Class name: org.apache.commons.collections.FastHashMap$Values
[ERROR] Found in: 
[ERROR] 	commons-beanutils-core-1.7.0.jar
[ERROR] 	commons-collections-2.1.jar
[ERROR] ----------------------------------
[ERROR] 
[ERROR] ---------------------------
[ERROR] -- Missing imports found --
[ERROR] ---------------------------
[ERROR] Missing classes: 
[ERROR] 	org.apache.log.Hierarchy
[ERROR] 		Imported by: 
[ERROR] 		 commons-logging-1.0.3.jar:org.apache.commons.logging.impl.LogKitLogger
[ERROR] 	org.apache.log.Logger
[ERROR] 		Imported by: 
[ERROR] 		 commons-logging-1.0.3.jar:org.apache.commons.logging.impl.LogKitLogger
[ERROR] 	org.apache.log4j.Category
[ERROR] 		Imported by: 
[ERROR] 		 commons-logging-1.0.3.jar:org.apache.commons.logging.impl.Log4JCategoryLog
[ERROR] 	org.apache.log4j.Logger
[ERROR] 		Imported by: 
[ERROR] 		 commons-logging-1.0.3.jar:org.apache.commons.logging.impl.Log4jFactory
[ERROR] 		 commons-logging-1.0.3.jar:org.apache.commons.logging.impl.Log4JLogger
[ERROR] 	org.apache.log4j.Priority
[ERROR] 		Imported by: 
[ERROR] 		 commons-logging-1.0.3.jar:org.apache.commons.logging.impl.Log4JLogger
[ERROR] 		 commons-logging-1.0.3.jar:org.apache.commons.logging.impl.Log4JCategoryLog
[ERROR] 
[INFO] --------------------------------
[INFO] -- All bundles are compatible --
[INFO] --------------------------------
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1:25.773s
[INFO] Finished at: Tue Feb 04 10:13:53 CET 2014
[INFO] Final Memory: 402M/1013M
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal cz.slezacek.ccp3:compatibility-checking-plugin:0.0.1:check (default-cli) on project checkstyle: Compatibility problems were found. Details above. -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException
