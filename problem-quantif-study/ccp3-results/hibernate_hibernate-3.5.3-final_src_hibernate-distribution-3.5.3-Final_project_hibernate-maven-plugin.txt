[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for org.hibernate:hibernate-maven-plugin:maven-plugin:3.5.3-Final
[WARNING] The expression ${groupId} is deprecated. Please use ${project.groupId} instead.
[WARNING] The expression ${version} is deprecated. Please use ${project.version} instead.
[WARNING] The expression ${inceptionYear} is deprecated. Please use ${project.inceptionYear} instead.
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate Maven Plugin 3.5.3-Final
[INFO] ------------------------------------------------------------------------
[WARNING] The POM for commons-logging:commons-logging:jar:99.0-does-not-exist is missing, no dependency information available
[WARNING] The POM for commons-logging:commons-logging-api:jar:99.0-does-not-exist is missing, no dependency information available
[WARNING] The POM for hsqldb:hsqldb:jar:1.8.0.2 is missing, no dependency information available
[INFO] 
[INFO] --- compatibility-checking-plugin:0.0.1:check (default-cli) @ hibernate-maven-plugin ---
[INFO] Processing project Hibernate Maven Plugin
[INFO] Adding artifact hibernate-core for processing
[INFO] Adding artifact antlr for processing
[INFO] Adding artifact commons-collections for processing
[INFO] Adding artifact dom4j for processing
[INFO] Adding artifact xml-apis for processing
[INFO] Adding artifact jta for processing
[INFO] Adding artifact maven-plugin-api for processing
[INFO] Adding artifact maven-project for processing
[INFO] Adding artifact maven-settings for processing
[INFO] Adding artifact maven-profile for processing
[INFO] Adding artifact maven-model for processing
[INFO] Adding artifact maven-artifact-manager for processing
[INFO] Adding artifact maven-repository-metadata for processing
[INFO] Adding artifact wagon-provider-api for processing
[INFO] Adding artifact maven-plugin-registry for processing
[INFO] Adding artifact plexus-utils for processing
[INFO] Adding artifact maven-artifact for processing
[INFO] Adding artifact plexus-container-default for processing
[INFO] Adding artifact classworlds for processing
[INFO] Adding artifact slf4j-api for processing
[WARNING] Artifact org.hibernate:hibernate-maven-plugin:maven-plugin:3.5.3-Final with type maven-plugin is not accepted
[INFO] Adding jars from JAVA_HOME
[INFO] Adding /opt/jdk1.7/jre/lib/rt.jar
[INFO] Processing artifacts compatibility check. This may take some time, please be patient.
[INFO] Loading classes into memory from byte code
[INFO] Analyzing compatibility problems
[WARNING] --------------------------
[WARNING] -- Unused imports found --
[WARNING] --------------------------
[WARNING] 	maven-artifact-manager-2.0.9.jar
[WARNING] 	wagon-provider-api-1.0-beta-2.jar
[WARNING] 	commons-collections-3.1.jar
[WARNING] 	hibernate-core-3.5.3-Final.jar
[WARNING] 	slf4j-api-1.5.8.jar
[WARNING] 	maven-project-2.0.9.jar
[WARNING] 	antlr-2.7.6.jar
[WARNING] 	dom4j-1.6.1.jar
[WARNING] 	xml-apis-1.0.b2.jar
[WARNING] 	maven-model-2.0.9.jar
[WARNING] 	plexus-container-default-1.0-alpha-9-stable-1.jar
[WARNING] 	plexus-utils-1.5.1.jar
[WARNING] 	maven-profile-2.0.9.jar
[WARNING] 	jta-1.1.jar
[WARNING] 	maven-settings-2.0.9.jar
[WARNING] 	maven-artifact-2.0.9.jar
[WARNING] 	classworlds-1.1-alpha-2.jar
[WARNING] 	maven-plugin-api-2.0.9.jar
[WARNING] 	maven-repository-metadata-2.0.9.jar
[WARNING] 	maven-plugin-registry-2.0.9.jar
[WARNING] 
[INFO] -------------------------------------
[INFO] -- No conficting class names found --
[INFO] -------------------------------------
[ERROR] ---------------------------
[ERROR] -- Missing imports found --
[ERROR] ---------------------------
[ERROR] Missing classes: 
[ERROR] 	com.sun.msv.datatype.xsd.TypeIncubator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	javassist.ClassPath
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.TransformingClassLoader
[ERROR] 	org.slf4j.impl.StaticMarkerBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.5.8.jar:org.slf4j.MarkerFactory
[ERROR] 	javassist.util.proxy.RuntimeSupport
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.BulkAccessorFactory
[ERROR] 	org.gjt.xpp.XmlEndTag
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	javassist.CtClass
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.TransformingClassLoader
[ERROR] 	net.sf.cglib.transform.impl.InterceptFieldTransformer
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	net.sf.cglib.transform.impl.InterceptFieldCallback
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.intercept.cglib.CGLIBHelper
[ERROR] 	net.sf.cglib.transform.ClassTransformer
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	javassist.util.proxy.MethodFilter
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.ProxyFactoryFactoryImpl$1
[ERROR] 	org.jaxen.pattern.Pattern
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.msv.datatype.xsd.XSDatatypeImpl
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	org.apache.tools.ant.DirectoryScanner
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	javassist.NotFoundException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.TransformingClassLoader
[ERROR] 	javax.security.jacc.EJBMethodPermission
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPreInsertEventListener
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPreLoadEventListener
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPreDeleteEventListener
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPreUpdateEventListener
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	net.sf.cglib.transform.impl.InterceptFieldFilter
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer$1
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	org.xmlpull.v1.XmlPullParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	junit.framework.TestCase
[ERROR] 		Imported by: 
[ERROR] 		 plexus-container-default-1.0-alpha-9-stable-1.jar:org.codehaus.plexus.PlexusTestCase
[ERROR] 	org.xmlpull.v1.XmlPullParserException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	javassist.bytecode.Descriptor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	org.objectweb.asm.ClassWriter
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	org.slf4j.impl.StaticLoggerBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.5.8.jar:org.slf4j.LoggerFactory
[ERROR] 	org.jaxen.saxpath.SAXPathException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.Context
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.msv.datatype.SerializationContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	junit.framework.Assert
[ERROR] 		Imported by: 
[ERROR] 		 plexus-container-default-1.0-alpha-9-stable-1.jar:org.codehaus.plexus.PlexusTestCase
[ERROR] 	net.sf.cglib.core.DebuggingClassWriter
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	org.jaxen.ContextSupport
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.XPathFunctionContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	javax.security.jacc.PolicyConfiguration
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	net.sf.cglib.beans.BulkBean
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.AccessOptimizerAdapter
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.BytecodeProviderImpl
[ERROR] 	javassist.bytecode.Bytecode
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.BulkAccessorFactory
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	com.sun.msv.datatype.DatabindableDatatype
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	org.gjt.xpp.XmlPullParserException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpp.ProxyXmlStartTag
[ERROR] 	org.jaxen.dom4j.Dom4jXPath
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 	net.sf.cglib.reflect.FastClass
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.InstantiationOptimizerAdapter
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.BytecodeProviderImpl
[ERROR] 	org.objectweb.asm.Type
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer$1
[ERROR] 	org.xmlpull.v1.XmlPullParserFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	javassist.bytecode.ConstPool
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.BulkAccessorFactory
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	javax.security.jacc.PolicyContextException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$2
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1$1
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	javassist.ClassPool
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.TransformingClassLoader
[ERROR] 	javax.security.jacc.PolicyConfigurationFactory
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCConfiguration
[ERROR] 	javassist.util.proxy.FactoryHelper
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.BulkAccessorFactory
[ERROR] 	org.slf4j.impl.StaticMDCBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.5.8.jar:org.slf4j.MDC
[ERROR] 	org.apache.tools.ant.BuildException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.apache.tools.ant.Project
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.gjt.xpp.XmlStartTag
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpp.ProxyXmlStartTag
[ERROR] 	org.jaxen.pattern.PatternParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.apache.tools.ant.types.FileSet
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaValidatorTask
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaUpdateTask
[ERROR] 	org.relaxng.datatype.ValidationContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	net.sf.cglib.transform.ClassReaderGenerator
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	javassist.bytecode.MethodInfo
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.BulkAccessorFactory
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	javassist.util.proxy.ProxyFactory
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.ProxyFactoryFactoryImpl$BasicProxyFactoryImpl
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer
[ERROR] 	net.sf.cglib.proxy.Factory
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.ProxyFactoryFactoryImpl$BasicProxyFactoryImpl
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.ProxyFactoryFactoryImpl$PassThroughInterceptor
[ERROR] 	org.jaxen.SimpleVariableContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	javassist.bytecode.CodeIterator
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	org.apache.tools.ant.Task
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.instrument.BasicInstrumentationTask
[ERROR] 	org.objectweb.asm.ClassVisitor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	net.sf.cglib.transform.TransformingClassGenerator
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	net.sf.cglib.proxy.Callback
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.ProxyFactoryFactoryImpl$BasicProxyFactoryImpl
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.ProxyFactoryFactoryImpl$PassThroughInterceptor
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.proxy.pojo.cglib.CGLIBLazyInitializer
[ERROR] 	org.jaxen.FunctionContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.XPath
[ERROR] 	javax.security.jacc.PolicyContext
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$2
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions$PolicyContextActions$1$1
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.secure.JACCPermissions$SetContextID
[ERROR] 	net.sf.cglib.proxy.MethodInterceptor
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.ProxyFactoryFactoryImpl$PassThroughInterceptor
[ERROR] 	javassist.util.proxy.MethodHandler
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.ProxyFactoryFactoryImpl$BasicProxyFactoryImpl
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.ProxyFactoryFactoryImpl$PassThroughHandler
[ERROR] 	org.jaxen.JaxenException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	net.sf.cglib.proxy.NoOp
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.ProxyFactoryFactoryImpl$BasicProxyFactoryImpl
[ERROR] 	org.jaxen.dom4j.DocumentNavigator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	net.sf.cglib.beans.BulkBeanException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.AccessOptimizerAdapter
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.BytecodeProviderImpl
[ERROR] 	net.sf.cglib.proxy.Enhancer
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.ProxyFactoryFactoryImpl$BasicProxyFactoryImpl
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.proxy.pojo.cglib.CGLIBLazyInitializer
[ERROR] 	org.gjt.xpp.XmlPullParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	javassist.bytecode.FieldInfo
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	org.gjt.xpp.XmlPullParserFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	net.sf.cglib.transform.impl.InterceptFieldEnabled
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.intercept.cglib.CGLIBHelper
[ERROR] 	net.sf.cglib.core.ClassNameReader
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.buildtime.CGLIBInstrumenter$CustomClassDescriptor
[ERROR] 	org.jaxen.NamespaceContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultNamespaceContext
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	javassist.bytecode.CodeAttribute
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.BulkAccessorFactory
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	org.jaxen.SimpleNamespaceContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.Navigator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.objectweb.asm.ClassReader
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.buildtime.CGLIBInstrumenter$CustomClassDescriptor
[ERROR] 	javassist.bytecode.BadBytecode
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	javassist.CannotCompileException
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.TransformingClassLoader
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	org.jaxen.VariableContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.DocumentHelper
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.DocumentFactory
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.XPath
[ERROR] 	net.sf.cglib.proxy.CallbackFilter
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.proxy.pojo.cglib.CGLIBLazyInitializer
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.proxy.pojo.cglib.CGLIBLazyInitializer$1
[ERROR] 	javassist.util.proxy.ProxyObject
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.ProxyFactoryFactoryImpl$BasicProxyFactoryImpl
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer
[ERROR] 	net.sf.cglib.proxy.InvocationHandler
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.proxy.pojo.cglib.CGLIBLazyInitializer
[ERROR] 	javassist.bytecode.ClassFile
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.buildtime.JavassistInstrumenter$CustomClassDescriptor
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.JavassistClassTransformer
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.JavassistClassTransformer$1
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.BulkAccessorFactory
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.javassist.FieldTransformer
[ERROR] 	org.relaxng.datatype.DatatypeException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	com.sun.msv.datatype.xsd.XSDatatype
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.NamedTypeResolver
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElementFactory
[ERROR] 	org.apache.tools.ant.taskdefs.MatchingTask
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.tool.hbm2ddl.SchemaExportTask
[ERROR] 	net.sf.cglib.core.ClassGenerator
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.CglibClassTransformer
[ERROR] 	com.sun.msv.datatype.xsd.DatatypeFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	org.jaxen.XPath
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.rule.Stylesheet
[ERROR] 	net.sf.cglib.proxy.MethodProxy
[ERROR] 		Imported by: 
[ERROR] 		 hibernate-core-3.5.3-Final.jar:org.hibernate.bytecode.cglib.ProxyFactoryFactoryImpl$PassThroughInterceptor
[ERROR] 
[INFO] --------------------------------
[INFO] -- All bundles are compatible --
[INFO] --------------------------------
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1:28.561s
[INFO] Finished at: Tue Feb 04 13:26:02 CET 2014
[INFO] Final Memory: 446M/1141M
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal cz.slezacek.ccp3:compatibility-checking-plugin:0.0.1:check (default-cli) on project hibernate-maven-plugin: Compatibility problems were found. Details above. -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException
