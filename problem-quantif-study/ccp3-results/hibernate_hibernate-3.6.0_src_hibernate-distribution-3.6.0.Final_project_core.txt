[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for org.hibernate:hibernate-core:jar:3.6.0.Final
[WARNING] The expression ${inceptionYear} is deprecated. Please use ${project.inceptionYear} instead.
[WARNING] The expression ${pom.version} is deprecated. Please use ${project.version} instead.
[WARNING] 'reporting.plugins.plugin.version' for org.apache.maven.plugins:maven-javadoc-plugin is missing. @ line 204, column 21
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate Core 3.6.0.Final
[INFO] ------------------------------------------------------------------------
[WARNING] The POM for commons-logging:commons-logging:jar:99.0-does-not-exist is missing, no dependency information available
[WARNING] The POM for commons-logging:commons-logging-api:jar:99.0-does-not-exist is missing, no dependency information available
[INFO] 
[INFO] --- compatibility-checking-plugin:0.0.1:check (default-cli) @ hibernate-core ---
[INFO] Processing project Hibernate Core
[INFO] Adding artifact antlr for processing
[INFO] Adding artifact commons-collections for processing
[INFO] Adding artifact dom4j for processing
[INFO] Adding artifact hibernate-commons-annotations for processing
[INFO] Adding artifact hibernate-jpa-2.0-api for processing
[INFO] Adding artifact validation-api for processing
[INFO] Adding artifact javassist for processing
[INFO] Adding artifact cglib for processing
[INFO] Adding artifact asm for processing
[INFO] Adding artifact jta for processing
[INFO] Adding artifact jboss-jacc-api_JDK4 for processing
[INFO] Adding artifact ant for processing
[INFO] Adding artifact slf4j-api for processing
[ERROR] Cannot find file for artifact: org.hibernate:hibernate-core:jar:3.6.0.Final
[INFO] Adding jars from JAVA_HOME
[INFO] Adding /opt/jdk1.7/jre/lib/rt.jar
[INFO] Processing artifacts compatibility check. This may take some time, please be patient.
[INFO] Loading classes into memory from byte code
[INFO] Analyzing compatibility problems
[WARNING] --------------------------
[WARNING] -- Unused imports found --
[WARNING] --------------------------
[WARNING] 	commons-collections-3.1.jar
[WARNING] 	ant-1.6.5.jar
[WARNING] 	hibernate-commons-annotations-3.2.0.Final.jar
[WARNING] 	javassist-3.12.0.GA.jar
[WARNING] 	antlr-2.7.6.jar
[WARNING] 	dom4j-1.6.1.jar
[WARNING] 	hibernate-jpa-2.0-api-1.0.0.Final.jar
[WARNING] 	cglib-2.2.jar
[WARNING] 	validation-api-1.0.0.GA.jar
[WARNING] 	asm-3.1.jar
[WARNING] 	jta-1.1.jar
[WARNING] 	jboss-jacc-api_JDK4-1.1.0.jar
[WARNING] 	slf4j-api-1.6.1.jar
[WARNING] 
[INFO] -------------------------------------
[INFO] -- No conficting class names found --
[INFO] -------------------------------------
[ERROR] ---------------------------
[ERROR] -- Missing imports found --
[ERROR] ---------------------------
[ERROR] Missing classes: 
[ERROR] 	com.sun.msv.datatype.xsd.TypeIncubator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	org.slf4j.impl.StaticMarkerBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.6.1.jar:org.slf4j.MarkerFactory
[ERROR] 	org.apache.tools.ant.launch.AntMain
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.Main
[ERROR] 	org.gjt.xpp.XmlEndTag
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	com.sun.jdi.request.EventRequestManager
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.jdi.connect.Connector$Argument
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.jdi.connect.IllegalConnectorArgumentsException
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.jaxen.pattern.Pattern
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.jdi.event.Event
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	org.apache.xml.resolver.helpers.PublicId
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.types.resolver.ApacheCatalog
[ERROR] 	com.sun.msv.datatype.xsd.XSDatatypeImpl
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	com.sun.jdi.connect.Connector
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.apache.xml.resolver.tools.CatalogResolver
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.types.resolver.ApacheCatalogResolver
[ERROR] 	org.xmlpull.v1.XmlPullParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	org.xmlpull.v1.XmlPullParserException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	org.apache.xml.resolver.Catalog
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.types.resolver.ApacheCatalog
[ERROR] 	org.slf4j.impl.StaticLoggerBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.6.1.jar:org.slf4j.LoggerFactory
[ERROR] 	org.apache.bsf.BSFException
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.util.ScriptRunner
[ERROR] 	org.apache.xml.resolver.helpers.Debug
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.types.resolver.ApacheCatalog
[ERROR] 	org.jaxen.Context
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.saxpath.SAXPathException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.jdi.event.MethodEntryEvent
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	com.sun.msv.datatype.SerializationContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	org.apache.bcel.classfile.ConstantValue
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.filters.util.JavaClassHelper
[ERROR] 	org.jaxen.ContextSupport
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.XPathFunctionContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.msv.datatype.DatabindableDatatype
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	com.sun.jdi.event.EventSet
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	org.gjt.xpp.XmlPullParserException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpp.ProxyXmlStartTag
[ERROR] 	org.jaxen.dom4j.Dom4jXPath
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 	com.sun.jdi.Bootstrap
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.jdi.event.EventIterator
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper$1
[ERROR] 	org.xmlpull.v1.XmlPullParserFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPP3Reader
[ERROR] 	org.slf4j.impl.StaticMDCBinder
[ERROR] 		Imported by: 
[ERROR] 		 slf4j-api-1.6.1.jar:org.slf4j.MDC
[ERROR] 	org.objectweb.asm.util.TraceClassVisitor
[ERROR] 		Imported by: 
[ERROR] 		 cglib-2.2.jar:net.sf.cglib.core.DebuggingClassWriter$1
[ERROR] 	com.sun.jdi.request.MethodEntryRequest
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.jaxen.pattern.PatternParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.relaxng.datatype.ValidationContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	org.gjt.xpp.XmlStartTag
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpp.ProxyXmlStartTag
[ERROR] 	com.sun.jdi.VirtualMachine
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.apache.xml.resolver.CatalogManager
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.types.resolver.ApacheCatalog
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.types.resolver.ApacheCatalogResolver
[ERROR] 	org.apache.bcel.classfile.Field
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.filters.util.JavaClassHelper
[ERROR] 	org.jaxen.SimpleVariableContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.jdi.ReferenceType
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.jaxen.FunctionContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.XPath
[ERROR] 	com.sun.jdi.request.EventRequest
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.apache.bcel.classfile.JavaClass
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.filters.util.JavaClassHelper
[ERROR] 	org.jboss.util.id.SerialVersion
[ERROR] 		Imported by: 
[ERROR] 		 jboss-jacc-api_JDK4-1.1.0.jar:javax.security.jacc.WebUserDataPermission
[ERROR] 		 jboss-jacc-api_JDK4-1.1.0.jar:javax.security.jacc.EJBMethodPermission
[ERROR] 		 jboss-jacc-api_JDK4-1.1.0.jar:javax.security.jacc.WebResourcePermission
[ERROR] 		 jboss-jacc-api_JDK4-1.1.0.jar:javax.security.jacc.WebRoleRefPermission
[ERROR] 		 jboss-jacc-api_JDK4-1.1.0.jar:javax.security.jacc.EJBRoleRefPermission
[ERROR] 	org.jaxen.JaxenException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.dom4j.DocumentNavigator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.gjt.xpp.XmlPullParser
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	org.gjt.xpp.XmlPullParserFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.io.XPPReader
[ERROR] 	org.jaxen.NamespaceContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultNamespaceContext
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.SimpleNamespaceContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	org.jaxen.Navigator
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 	com.sun.jdi.connect.AttachingConnector
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.jaxen.VariableContext
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.DocumentHelper
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.DocumentFactory
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.XPathPattern
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.XPath
[ERROR] 	org.apache.bsf.BSFManager
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.util.ScriptRunner
[ERROR] 	org.relaxng.datatype.DatatypeException
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 	org.apache.bcel.classfile.ClassParser
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.filters.util.JavaClassHelper
[ERROR] 	com.sun.msv.datatype.xsd.XSDatatype
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeAttribute
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElement
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.NamedTypeResolver
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.DatatypeElementFactory
[ERROR] 	org.apache.tools.ant.launch.Locator
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.util.FileUtils
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.util.LoaderUtils
[ERROR] 	javax.servlet.http.HttpServletRequest
[ERROR] 		Imported by: 
[ERROR] 		 jboss-jacc-api_JDK4-1.1.0.jar:javax.security.jacc.WebUserDataPermission
[ERROR] 		 jboss-jacc-api_JDK4-1.1.0.jar:javax.security.jacc.WebResourcePermission
[ERROR] 	com.sun.jdi.VirtualMachineManager
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	com.sun.msv.datatype.xsd.DatatypeFactory
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.datatype.SchemaParser
[ERROR] 	org.jaxen.XPath
[ERROR] 		Imported by: 
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.xpath.DefaultXPath
[ERROR] 		 dom4j-1.6.1.jar:org.dom4j.rule.Stylesheet
[ERROR] 	com.sun.jdi.event.EventQueue
[ERROR] 		Imported by: 
[ERROR] 		 javassist-3.12.0.GA.jar:javassist.util.HotSwapper
[ERROR] 	org.apache.xml.resolver.CatalogEntry
[ERROR] 		Imported by: 
[ERROR] 		 ant-1.6.5.jar:org.apache.tools.ant.types.resolver.ApacheCatalog
[ERROR] 
[INFO] --------------------------------
[INFO] -- All bundles are compatible --
[INFO] --------------------------------
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1:31.694s
[INFO] Finished at: Tue Feb 04 14:04:20 CET 2014
[INFO] Final Memory: 427M/1143M
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal cz.slezacek.ccp3:compatibility-checking-plugin:0.0.1:check (default-cli) on project hibernate-core: Compatibility problems were found. Details above. -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException
