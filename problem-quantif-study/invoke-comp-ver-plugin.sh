#!/bin/bash

CORPUS_PATH=/home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/


for file in enforcer-results/*.txt; do

    fileName=$(head -n 1 $file)
    fileName=${fileName//File: /};

    relPath=${fileName//$CORPUS_PATH/};
    resultFile=${relPath//\//_};
    resultFile=${resultFile//_pom.xml/};
  
    resultFile="ccp3-results/"$resultFile".txt"

    echo $fileName

    mvn cz.slezacek.ccp3:compatibility-checking-plugin:check  -f $fileName > "$resultFile"
done


