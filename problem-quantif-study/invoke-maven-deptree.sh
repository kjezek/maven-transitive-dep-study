#!/bin/bash

CORPUS_PATH=/home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/


while read file; do

  relPath=${file//$CORPUS_PATH/};
  resultFile=${relPath//\//_};
  resultFile=${resultFile//_pom.xml/};
  
  resultFile="deptree-results/"$resultFile".txt"
  
  echo $file
  echo "File: "$file"" > "$resultFile"
  
   # invoke maven enforcer plugin
  mvn dependency:tree -f $file >> "$resultFile"
done < maven-projects.txt