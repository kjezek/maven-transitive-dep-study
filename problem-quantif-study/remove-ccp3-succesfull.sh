#/bin/bash

for f in ccp3-results/*.txt
do

  # remove files not having the enforcer error message
  if grep --quiet "BUILD FAILURE" $f; then
    echo $f
  else   
    rm $f
  fi  
  
done