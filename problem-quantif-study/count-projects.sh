#!/bin/bash

CORPUS_PATH=/home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/

TMP1=counts1.tmp
TMP2=counts2.tmp


while read file; do

  relPath=${file//$CORPUS_PATH/};
  resultFile=${relPath//\//_};
  resultFile=${resultFile//_pom.xml/};

  resultFileArray=(${resultFile//_/ });

  echo ${resultFileArray[0]} >> $TMP1
  echo ${resultFileArray[1]} >> $TMP2
  
done < maven-projects.txt

echo "Number of projects with at lest one Maven module"
cat $TMP1 | sort | uniq | wc -l


echo "Number of projects versions with at lest one Maven module"
cat $TMP2 | sort | uniq | wc -l

rm $TMP1
rm $TMP2
