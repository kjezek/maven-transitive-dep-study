[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO] 
[INFO] Hibernate Getting Started Guide Tutorials
[INFO] Hibernate hbm.xml Tutorial
[INFO] Hibernate Annotations Tutorial
[INFO] Hibernate JPA Tutorial
[INFO] Hibernate Envers Tutorial
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate Getting Started Guide Tutorials 3.6.3.Final
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-install-plugin:2.3.1:install (default-install) @ hibernate-tutorials ---
[INFO] Installing /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.3/src/hibernate-distribution-3.6.3.Final/project/hibernate-documentation/quickstart/tutorials/pom.xml to /home/kamilos/.m2/repository/org/hibernate/tutorials/hibernate-tutorials/3.6.3.Final/hibernate-tutorials-3.6.3.Final.pom
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate hbm.xml Tutorial 3.6.3.Final
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.5:resources (default-resources) @ hibernate-tutorial-hbm ---
[debug] execute contextualize
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.3/src/hibernate-distribution-3.6.3.Final/project/hibernate-documentation/quickstart/tutorials/basic/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:compile (default-compile) @ hibernate-tutorial-hbm ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:2.5:testResources (default-testResources) @ hibernate-tutorial-hbm ---
[debug] execute contextualize
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.3/src/hibernate-distribution-3.6.3.Final/project/hibernate-documentation/quickstart/tutorials/basic/src/main/java
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:testCompile (default-testCompile) @ hibernate-tutorial-hbm ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.10:test (default-test) @ hibernate-tutorial-hbm ---
[INFO] Surefire report directory: /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.3/src/hibernate-distribution-3.6.3.Final/project/hibernate-documentation/quickstart/tutorials/basic/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running org.hibernate.tutorial.hbm.NativeApiIllustrationTest
82 [main] INFO org.hibernate.annotations.common.Version - Hibernate Commons Annotations 3.2.0.Final
86 [main] INFO org.hibernate.cfg.Environment - Hibernate 3.6.3.Final
87 [main] INFO org.hibernate.cfg.Environment - hibernate.properties not found
90 [main] INFO org.hibernate.cfg.Environment - Bytecode provider name : javassist
93 [main] INFO org.hibernate.cfg.Environment - using JDK 1.4 java.sql.Timestamp handling
137 [main] INFO org.hibernate.cfg.Configuration - configuring from resource: /hibernate.cfg.xml
137 [main] INFO org.hibernate.cfg.Configuration - Configuration resource: /hibernate.cfg.xml
192 [main] INFO org.hibernate.cfg.Configuration - Reading mappings from resource : org/hibernate/tutorial/hbm/Event.hbm.xml
Tests run: 1, Failures: 0, Errors: 1, Skipped: 0, Time elapsed: 0.24 sec <<< FAILURE!

Results :

Tests in error: 
  testBasicUsage(org.hibernate.tutorial.hbm.NativeApiIllustrationTest): resource: org/hibernate/tutorial/hbm/Event.hbm.xml not found

Tests run: 1, Failures: 0, Errors: 1, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO] 
[INFO] Hibernate Getting Started Guide Tutorials ......... SUCCESS [0.308s]
[INFO] Hibernate hbm.xml Tutorial ........................ FAILURE [1.014s]
[INFO] Hibernate Annotations Tutorial .................... SKIPPED
[INFO] Hibernate JPA Tutorial ............................ SKIPPED
[INFO] Hibernate Envers Tutorial ......................... SKIPPED
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1.450s
[INFO] Finished at: Fri Jan 31 00:45:36 CET 2014
[INFO] Final Memory: 7M/150M
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal org.apache.maven.plugins:maven-surefire-plugin:2.10:test (default-test) on project hibernate-tutorial-hbm: There are test failures.
[ERROR] 
[ERROR] Please refer to /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.3/src/hibernate-distribution-3.6.3.Final/project/hibernate-documentation/quickstart/tutorials/basic/target/surefire-reports for the individual test results.
[ERROR] -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoFailureException
[ERROR] 
[ERROR] After correcting the problems, you can resume the build with the command
[ERROR]   mvn <goals> -rf :hibernate-tutorial-hbm
