[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate Annotations Tutorial 3.6.9.Final
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.5:resources (default-resources) @ hibernate-tutorial-annotations ---
[debug] execute contextualize
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.9/src/hibernate-distribution-3.6.9.Final/project/hibernate-documentation/quickstart/tutorials/annotations/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:compile (default-compile) @ hibernate-tutorial-annotations ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:2.5:testResources (default-testResources) @ hibernate-tutorial-annotations ---
[debug] execute contextualize
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.9/src/hibernate-distribution-3.6.9.Final/project/hibernate-documentation/quickstart/tutorials/annotations/src/main/java
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:testCompile (default-testCompile) @ hibernate-tutorial-annotations ---
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 2 source files to /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.9/src/hibernate-distribution-3.6.9.Final/project/hibernate-documentation/quickstart/tutorials/annotations/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.10:test (default-test) @ hibernate-tutorial-annotations ---
[INFO] Surefire report directory: /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.9/src/hibernate-distribution-3.6.9.Final/project/hibernate-documentation/quickstart/tutorials/annotations/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running org.hibernate.tutorial.annotations.AnnotationsIllustrationTest
81 [main] INFO org.hibernate.annotations.common.Version - Hibernate Commons Annotations 3.2.0.Final
86 [main] INFO org.hibernate.cfg.Environment - Hibernate 3.6.9.Final
87 [main] INFO org.hibernate.cfg.Environment - hibernate.properties not found
89 [main] INFO org.hibernate.cfg.Environment - Bytecode provider name : javassist
92 [main] INFO org.hibernate.cfg.Environment - using JDK 1.4 java.sql.Timestamp handling
137 [main] INFO org.hibernate.cfg.Configuration - configuring from resource: /hibernate.cfg.xml
137 [main] INFO org.hibernate.cfg.Configuration - Configuration resource: /hibernate.cfg.xml
198 [main] INFO org.hibernate.cfg.Configuration - Configured SessionFactory: null
242 [main] INFO org.hibernate.cfg.AnnotationBinder - Binding entity from annotated class: org.hibernate.tutorial.annotations.Event
271 [main] INFO org.hibernate.cfg.annotations.EntityBinder - Bind entity org.hibernate.tutorial.annotations.Event on table EVENTS
317 [main] INFO org.hibernate.cfg.Configuration - Hibernate Validator not found: ignoring
321 [main] INFO org.hibernate.cfg.search.HibernateSearchEventListenerRegister - Unable to find org.hibernate.search.event.FullTextIndexEventListener on the classpath. Hibernate Search is not enabled.
325 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - Using Hibernate built-in connection pool (not for production use!)
325 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - Hibernate connection pool size: 1
325 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - autocommit mode: false
328 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - using driver: org.h2.Driver at URL: jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1;MVCC=TRUE
329 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - connection properties: {user=sa, password=****}
462 [main] INFO org.hibernate.dialect.Dialect - Using dialect: org.hibernate.dialect.H2Dialect
477 [main] INFO org.hibernate.engine.jdbc.JdbcSupportLoader - Disabling contextual LOB creation as JDBC driver reported JDBC version [3] less than 4
477 [main] INFO org.hibernate.cfg.SettingsFactory - Database ->
       name : H2
    version : 1.2.140 (2010-07-25)
      major : 1
      minor : 2
477 [main] INFO org.hibernate.cfg.SettingsFactory - Driver ->
       name : H2 JDBC Driver
    version : 1.2.140 (2010-07-25)
      major : 1
      minor : 2
478 [main] INFO org.hibernate.transaction.TransactionFactoryFactory - Using default transaction strategy (direct JDBC transactions)
479 [main] INFO org.hibernate.transaction.TransactionManagerLookupFactory - No TransactionManagerLookup configured (in JTA environment, use of read-write or transactional second-level cache is not recommended)
479 [main] INFO org.hibernate.cfg.SettingsFactory - Automatic flush during beforeCompletion(): disabled
480 [main] INFO org.hibernate.cfg.SettingsFactory - Automatic session close at end of transaction: disabled
480 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC batch size: 15
480 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC batch updates for versioned data: disabled
480 [main] INFO org.hibernate.cfg.SettingsFactory - Scrollable result sets: enabled
480 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC3 getGeneratedKeys(): enabled
480 [main] INFO org.hibernate.cfg.SettingsFactory - Connection release mode: auto
480 [main] INFO org.hibernate.cfg.SettingsFactory - Default batch fetch size: 1
481 [main] INFO org.hibernate.cfg.SettingsFactory - Generate SQL with comments: disabled
481 [main] INFO org.hibernate.cfg.SettingsFactory - Order SQL updates by primary key: disabled
481 [main] INFO org.hibernate.cfg.SettingsFactory - Order SQL inserts for batching: disabled
481 [main] INFO org.hibernate.cfg.SettingsFactory - Query translator: org.hibernate.hql.ast.ASTQueryTranslatorFactory
482 [main] INFO org.hibernate.hql.ast.ASTQueryTranslatorFactory - Using ASTQueryTranslatorFactory
482 [main] INFO org.hibernate.cfg.SettingsFactory - Query language substitutions: {}
482 [main] INFO org.hibernate.cfg.SettingsFactory - JPA-QL strict compliance: disabled
482 [main] INFO org.hibernate.cfg.SettingsFactory - Second-level cache: enabled
482 [main] INFO org.hibernate.cfg.SettingsFactory - Query cache: disabled
482 [main] INFO org.hibernate.cfg.SettingsFactory - Cache region factory : org.hibernate.cache.impl.bridge.RegionFactoryCacheProviderBridge
485 [main] INFO org.hibernate.cache.impl.bridge.RegionFactoryCacheProviderBridge - Cache provider: org.hibernate.cache.NoCacheProvider
485 [main] INFO org.hibernate.cfg.SettingsFactory - Optimize cache for minimal puts: disabled
485 [main] INFO org.hibernate.cfg.SettingsFactory - Structured second-level cache entries: disabled
488 [main] INFO org.hibernate.cfg.SettingsFactory - Echoing all SQL to stdout
488 [main] INFO org.hibernate.cfg.SettingsFactory - Statistics: disabled
488 [main] INFO org.hibernate.cfg.SettingsFactory - Deleted entity synthetic identifier rollback: disabled
488 [main] INFO org.hibernate.cfg.SettingsFactory - Default entity-mode: pojo
488 [main] INFO org.hibernate.cfg.SettingsFactory - Named query checking : enabled
488 [main] INFO org.hibernate.cfg.SettingsFactory - Check Nullability in Core (should be disabled when Bean Validation is on): enabled
504 [main] INFO org.hibernate.impl.SessionFactoryImpl - building session factory
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [clob] overrides previous : org.hibernate.type.ClobType@7521491b
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [java.sql.Clob] overrides previous : org.hibernate.type.ClobType@7521491b
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [materialized_clob] overrides previous : org.hibernate.type.MaterializedClobType@5c46c7b0
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [wrapper_characters_clob] overrides previous : org.hibernate.type.CharacterArrayClobType@2ae02324
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [materialized_blob] overrides previous : org.hibernate.type.MaterializedBlobType@61232679
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [characters_clob] overrides previous : org.hibernate.type.PrimitiveCharacterArrayClobType@4ab303c1
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [blob] overrides previous : org.hibernate.type.BlobType@2ebb9a37
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [java.sql.Blob] overrides previous : org.hibernate.type.BlobType@2ebb9a37
508 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [wrapper_materialized_blob] overrides previous : org.hibernate.type.WrappedMaterializedBlobType@1e89a8d9
628 [main] INFO org.hibernate.impl.SessionFactoryObjectFactory - Not binding factory to JNDI, no JNDI name configured
633 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - Running hbm2ddl schema export
633 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - exporting generated schema to database
639 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - schema export complete
Hibernate: select max(id) from EVENTS
Hibernate: insert into EVENTS (EVENT_DATE, title, id) values (?, ?, ?)
Hibernate: insert into EVENTS (EVENT_DATE, title, id) values (?, ?, ?)
Hibernate: select event0_.id as id0_, event0_.EVENT_DATE as EVENT2_0_, event0_.title as title0_ from EVENTS event0_
Event (2014-01-31 00:47:55.825) : Our very first event!
Event (2014-01-31 00:47:55.847) : A follow up event
794 [main] INFO org.hibernate.impl.SessionFactoryImpl - closing
794 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - cleaning up connection pool: jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1;MVCC=TRUE
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.837 sec

Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.3.2:jar (default-jar) @ hibernate-tutorial-annotations ---
[WARNING] JAR will be empty - no content was marked for inclusion!
[INFO] Building jar: /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.9/src/hibernate-distribution-3.6.9.Final/project/hibernate-documentation/quickstart/tutorials/annotations/target/hibernate-tutorial-annotations-3.6.9.Final.jar
[INFO] 
[INFO] --- maven-install-plugin:2.3.1:install (default-install) @ hibernate-tutorial-annotations ---
[INFO] Installing /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.9/src/hibernate-distribution-3.6.9.Final/project/hibernate-documentation/quickstart/tutorials/annotations/target/hibernate-tutorial-annotations-3.6.9.Final.jar to /home/kamilos/.m2/repository/org/hibernate/tutorials/hibernate-tutorial-annotations/3.6.9.Final/hibernate-tutorial-annotations-3.6.9.Final.jar
[INFO] Installing /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.9/src/hibernate-distribution-3.6.9.Final/project/hibernate-documentation/quickstart/tutorials/annotations/pom.xml to /home/kamilos/.m2/repository/org/hibernate/tutorials/hibernate-tutorial-annotations/3.6.9.Final/hibernate-tutorial-annotations-3.6.9.Final.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 2.588s
[INFO] Finished at: Fri Jan 31 00:47:56 CET 2014
[INFO] Final Memory: 12M/150M
[INFO] ------------------------------------------------------------------------
