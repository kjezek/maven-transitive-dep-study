[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate Envers Tutorial 3.6.6.Final
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.5:resources (default-resources) @ hibernate-tutorial-envers ---
[debug] execute contextualize
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.6/src/hibernate-distribution-3.6.6.Final/project/hibernate-documentation/quickstart/tutorials/envers/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:compile (default-compile) @ hibernate-tutorial-envers ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:2.5:testResources (default-testResources) @ hibernate-tutorial-envers ---
[debug] execute contextualize
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.6/src/hibernate-distribution-3.6.6.Final/project/hibernate-documentation/quickstart/tutorials/envers/src/main/java
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:testCompile (default-testCompile) @ hibernate-tutorial-envers ---
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 2 source files to /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.6/src/hibernate-distribution-3.6.6.Final/project/hibernate-documentation/quickstart/tutorials/envers/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.10:test (default-test) @ hibernate-tutorial-envers ---
[INFO] Surefire report directory: /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.6/src/hibernate-distribution-3.6.6.Final/project/hibernate-documentation/quickstart/tutorials/envers/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running org.hibernate.tutorial.envers.EnversIllustrationTest
87 [main] INFO org.hibernate.annotations.common.Version - Hibernate Commons Annotations 3.2.0.Final
93 [main] INFO org.hibernate.cfg.Environment - Hibernate 3.6.6.Final
94 [main] INFO org.hibernate.cfg.Environment - hibernate.properties not found
96 [main] INFO org.hibernate.cfg.Environment - Bytecode provider name : javassist
100 [main] INFO org.hibernate.cfg.Environment - using JDK 1.4 java.sql.Timestamp handling
151 [main] INFO org.hibernate.ejb.Version - Hibernate EntityManager 3.6.6.Final
336 [main] INFO org.hibernate.cfg.AnnotationBinder - Binding entity from annotated class: org.hibernate.tutorial.envers.Event
362 [main] INFO org.hibernate.cfg.annotations.EntityBinder - Bind entity org.hibernate.tutorial.envers.Event on table EVENTS
403 [main] INFO org.hibernate.cfg.Configuration - Hibernate Validator not found: ignoring
422 [main] INFO org.hibernate.cfg.search.HibernateSearchEventListenerRegister - Unable to find org.hibernate.search.event.FullTextIndexEventListener on the classpath. Hibernate Search is not enabled.
426 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - Using Hibernate built-in connection pool (not for production use!)
426 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - Hibernate connection pool size: 20
426 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - autocommit mode: true
429 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - using driver: org.h2.Driver at URL: jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1;MVCC=TRUE
430 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - connection properties: {user=sa, autocommit=true, release_mode=auto}
558 [main] INFO org.hibernate.dialect.Dialect - Using dialect: org.hibernate.dialect.H2Dialect
566 [main] INFO org.hibernate.engine.jdbc.JdbcSupportLoader - Disabling contextual LOB creation as JDBC driver reported JDBC version [3] less than 4
566 [main] INFO org.hibernate.cfg.SettingsFactory - Database ->
       name : H2
    version : 1.2.140 (2010-07-25)
      major : 1
      minor : 2
566 [main] INFO org.hibernate.cfg.SettingsFactory - Driver ->
       name : H2 JDBC Driver
    version : 1.2.140 (2010-07-25)
      major : 1
      minor : 2
567 [main] INFO org.hibernate.transaction.TransactionFactoryFactory - Using default transaction strategy (direct JDBC transactions)
568 [main] INFO org.hibernate.transaction.TransactionManagerLookupFactory - No TransactionManagerLookup configured (in JTA environment, use of read-write or transactional second-level cache is not recommended)
568 [main] INFO org.hibernate.cfg.SettingsFactory - Automatic flush during beforeCompletion(): disabled
568 [main] INFO org.hibernate.cfg.SettingsFactory - Automatic session close at end of transaction: disabled
568 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC batch size: 15
568 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC batch updates for versioned data: disabled
569 [main] INFO org.hibernate.cfg.SettingsFactory - Scrollable result sets: enabled
569 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC3 getGeneratedKeys(): enabled
569 [main] INFO org.hibernate.cfg.SettingsFactory - Connection release mode: auto
569 [main] INFO org.hibernate.cfg.SettingsFactory - Default batch fetch size: 1
569 [main] INFO org.hibernate.cfg.SettingsFactory - Generate SQL with comments: disabled
569 [main] INFO org.hibernate.cfg.SettingsFactory - Order SQL updates by primary key: disabled
569 [main] INFO org.hibernate.cfg.SettingsFactory - Order SQL inserts for batching: disabled
569 [main] INFO org.hibernate.cfg.SettingsFactory - Query translator: org.hibernate.hql.ast.ASTQueryTranslatorFactory
570 [main] INFO org.hibernate.hql.ast.ASTQueryTranslatorFactory - Using ASTQueryTranslatorFactory
570 [main] INFO org.hibernate.cfg.SettingsFactory - Query language substitutions: {}
570 [main] INFO org.hibernate.cfg.SettingsFactory - JPA-QL strict compliance: enabled
571 [main] INFO org.hibernate.cfg.SettingsFactory - Second-level cache: enabled
571 [main] INFO org.hibernate.cfg.SettingsFactory - Query cache: disabled
571 [main] INFO org.hibernate.cfg.SettingsFactory - Cache region factory : org.hibernate.cache.impl.NoCachingRegionFactory
571 [main] INFO org.hibernate.cfg.SettingsFactory - Optimize cache for minimal puts: disabled
571 [main] INFO org.hibernate.cfg.SettingsFactory - Structured second-level cache entries: disabled
574 [main] INFO org.hibernate.cfg.SettingsFactory - Echoing all SQL to stdout
574 [main] INFO org.hibernate.cfg.SettingsFactory - Statistics: disabled
574 [main] INFO org.hibernate.cfg.SettingsFactory - Deleted entity synthetic identifier rollback: disabled
574 [main] INFO org.hibernate.cfg.SettingsFactory - Default entity-mode: pojo
574 [main] INFO org.hibernate.cfg.SettingsFactory - Named query checking : enabled
574 [main] INFO org.hibernate.cfg.SettingsFactory - Check Nullability in Core (should be disabled when Bean Validation is on): enabled
652 [main] INFO org.hibernate.cfg.HbmBinder - Mapping class: org.hibernate.tutorial.envers.Event_AUD -> EVENTS_AUD
659 [main] INFO org.hibernate.cfg.HbmBinder - Mapping class: org.hibernate.envers.DefaultRevisionEntity -> REVINFO
661 [main] INFO org.hibernate.impl.SessionFactoryImpl - building session factory
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [clob] overrides previous : org.hibernate.type.ClobType@4c352ca9
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [java.sql.Clob] overrides previous : org.hibernate.type.ClobType@4c352ca9
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [blob] overrides previous : org.hibernate.type.BlobType@37073255
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [java.sql.Blob] overrides previous : org.hibernate.type.BlobType@37073255
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [wrapper_materialized_blob] overrides previous : org.hibernate.type.WrappedMaterializedBlobType@397986ac
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [characters_clob] overrides previous : org.hibernate.type.PrimitiveCharacterArrayClobType@5978a3ae
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [materialized_blob] overrides previous : org.hibernate.type.MaterializedBlobType@7f422265
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [wrapper_characters_clob] overrides previous : org.hibernate.type.CharacterArrayClobType@4ee05628
665 [main] INFO org.hibernate.type.BasicTypeRegistry - Type registration [materialized_clob] overrides previous : org.hibernate.type.MaterializedClobType@6a38848c
778 [main] INFO org.hibernate.impl.SessionFactoryObjectFactory - Not binding factory to JNDI, no JNDI name configured
783 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - Running hbm2ddl schema export
784 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - exporting generated schema to database
1232 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - schema export complete
Hibernate: select max(id) from EVENTS
Hibernate: insert into EVENTS (EVENT_DATE, title, id) values (?, ?, ?)
Hibernate: insert into EVENTS (EVENT_DATE, title, id) values (?, ?, ?)
Hibernate: insert into REVINFO (REV, REVTSTMP) values (null, ?)
Hibernate: insert into EVENTS_AUD (REVTYPE, EVENT_DATE, title, id, REV) values (?, ?, ?, ?, ?)
Hibernate: insert into EVENTS_AUD (REVTYPE, EVENT_DATE, title, id, REV) values (?, ?, ?, ?, ?)
Hibernate: select event0_.id as id0_, event0_.EVENT_DATE as EVENT2_0_, event0_.title as title0_ from EVENTS event0_
Event (2014-01-31 00:46:49.034) : Our very first event!
Event (2014-01-31 00:46:49.052) : A follow up event
Hibernate: select event0_.id as id0_0_, event0_.EVENT_DATE as EVENT2_0_0_, event0_.title as title0_0_ from EVENTS event0_ where event0_.id=?
Hibernate: update EVENTS set EVENT_DATE=?, title=? where id=?
Hibernate: insert into REVINFO (REV, REVTSTMP) values (null, ?)
Hibernate: insert into EVENTS_AUD (REVTYPE, EVENT_DATE, title, id, REV) values (?, ?, ?, ?, ?)
Hibernate: select event0_.id as id0_0_, event0_.EVENT_DATE as EVENT2_0_0_, event0_.title as title0_0_ from EVENTS event0_ where event0_.id=?
Hibernate: select event_aud0_.id as id1_, event_aud0_.REV as REV1_, event_aud0_.REVTYPE as REVTYPE1_, event_aud0_.EVENT_DATE as EVENT4_1_, event_aud0_.title as title1_ from EVENTS_AUD event_aud0_ where event_aud0_.REV=(select max(event_aud1_.REV) from EVENTS_AUD event_aud1_ where event_aud1_.REV<=? and event_aud0_.id=event_aud1_.id) and event_aud0_.REVTYPE<>? and event_aud0_.id=?
Hibernate: select event_aud0_.id as id1_, event_aud0_.REV as REV1_, event_aud0_.REVTYPE as REVTYPE1_, event_aud0_.EVENT_DATE as EVENT4_1_, event_aud0_.title as title1_ from EVENTS_AUD event_aud0_ where event_aud0_.REV=(select max(event_aud1_.REV) from EVENTS_AUD event_aud1_ where event_aud1_.REV<=? and event_aud0_.id=event_aud1_.id) and event_aud0_.REVTYPE<>? and event_aud0_.id=?
1480 [main] INFO org.hibernate.impl.SessionFactoryImpl - closing
1480 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - cleaning up connection pool: jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1;MVCC=TRUE
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.542 sec

Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.3.2:jar (default-jar) @ hibernate-tutorial-envers ---
[WARNING] JAR will be empty - no content was marked for inclusion!
[INFO] Building jar: /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.6/src/hibernate-distribution-3.6.6.Final/project/hibernate-documentation/quickstart/tutorials/envers/target/hibernate-tutorial-envers-3.6.6.Final.jar
[INFO] 
[INFO] --- maven-install-plugin:2.3.1:install (default-install) @ hibernate-tutorial-envers ---
[INFO] Installing /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.6/src/hibernate-distribution-3.6.6.Final/project/hibernate-documentation/quickstart/tutorials/envers/target/hibernate-tutorial-envers-3.6.6.Final.jar to /home/kamilos/.m2/repository/org/hibernate/tutorials/hibernate-tutorial-envers/3.6.6.Final/hibernate-tutorial-envers-3.6.6.Final.jar
[INFO] Installing /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.6/src/hibernate-distribution-3.6.6.Final/project/hibernate-documentation/quickstart/tutorials/envers/pom.xml to /home/kamilos/.m2/repository/org/hibernate/tutorials/hibernate-tutorial-envers/3.6.6.Final/hibernate-tutorial-envers-3.6.6.Final.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.608s
[INFO] Finished at: Fri Jan 31 00:46:49 CET 2014
[INFO] Final Memory: 12M/150M
[INFO] ------------------------------------------------------------------------
