[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Hibernate JPA Tutorial 3.6.0.Final
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.5:resources (default-resources) @ hibernate-tutorial-entitymanager ---
[debug] execute contextualize
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.0/src/hibernate-distribution-3.6.0.Final/project/documentation/quickstart/tutorials/entitymanager/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:compile (default-compile) @ hibernate-tutorial-entitymanager ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:2.5:testResources (default-testResources) @ hibernate-tutorial-entitymanager ---
[debug] execute contextualize
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.0/src/hibernate-distribution-3.6.0.Final/project/documentation/quickstart/tutorials/entitymanager/src/main/java
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:testCompile (default-testCompile) @ hibernate-tutorial-entitymanager ---
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 2 source files to /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.0/src/hibernate-distribution-3.6.0.Final/project/documentation/quickstart/tutorials/entitymanager/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.10:test (default-test) @ hibernate-tutorial-entitymanager ---
[INFO] Surefire report directory: /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.0/src/hibernate-distribution-3.6.0.Final/project/documentation/quickstart/tutorials/entitymanager/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running org.hibernate.tutorial.em.EntityManagerIllustrationTest
75 [main] INFO org.hibernate.annotations.common.Version - Hibernate Commons Annotations 3.2.0.Final
80 [main] INFO org.hibernate.cfg.Environment - Hibernate 3.6.0.Final
81 [main] INFO org.hibernate.cfg.Environment - hibernate.properties not found
83 [main] INFO org.hibernate.cfg.Environment - Bytecode provider name : javassist
86 [main] INFO org.hibernate.cfg.Environment - using JDK 1.4 java.sql.Timestamp handling
133 [main] INFO org.hibernate.ejb.Version - Hibernate EntityManager 3.6.0.Final
314 [main] INFO org.hibernate.cfg.AnnotationBinder - Binding entity from annotated class: org.hibernate.tutorial.em.Event
341 [main] INFO org.hibernate.cfg.annotations.EntityBinder - Bind entity org.hibernate.tutorial.em.Event on table EVENTS
382 [main] INFO org.hibernate.cfg.Configuration - Hibernate Validator not found: ignoring
400 [main] INFO org.hibernate.cfg.search.HibernateSearchEventListenerRegister - Unable to find org.hibernate.search.event.FullTextIndexEventListener on the classpath. Hibernate Search is not enabled.
404 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - Using Hibernate built-in connection pool (not for production use!)
405 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - Hibernate connection pool size: 20
405 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - autocommit mode: true
408 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - using driver: org.h2.Driver at URL: jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1;MVCC=TRUE
408 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - connection properties: {user=sa, autocommit=true, release_mode=auto}
527 [main] INFO org.hibernate.cfg.SettingsFactory - Database ->
       name : H2
    version : 1.2.140 (2010-07-25)
      major : 1
      minor : 2
527 [main] INFO org.hibernate.cfg.SettingsFactory - Driver ->
       name : H2 JDBC Driver
    version : 1.2.140 (2010-07-25)
      major : 1
      minor : 2
540 [main] INFO org.hibernate.dialect.Dialect - Using dialect: org.hibernate.dialect.H2Dialect
547 [main] INFO org.hibernate.engine.jdbc.JdbcSupportLoader - Disabling contextual LOB creation as JDBC driver reported JDBC version [3] less than 4
548 [main] INFO org.hibernate.transaction.TransactionFactoryFactory - Transaction strategy: org.hibernate.transaction.JDBCTransactionFactory
549 [main] INFO org.hibernate.transaction.TransactionManagerLookupFactory - No TransactionManagerLookup configured (in JTA environment, use of read-write or transactional second-level cache is not recommended)
549 [main] INFO org.hibernate.cfg.SettingsFactory - Automatic flush during beforeCompletion(): disabled
549 [main] INFO org.hibernate.cfg.SettingsFactory - Automatic session close at end of transaction: disabled
549 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC batch size: 15
549 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC batch updates for versioned data: disabled
549 [main] INFO org.hibernate.cfg.SettingsFactory - Scrollable result sets: enabled
549 [main] INFO org.hibernate.cfg.SettingsFactory - JDBC3 getGeneratedKeys(): enabled
549 [main] INFO org.hibernate.cfg.SettingsFactory - Connection release mode: auto
550 [main] INFO org.hibernate.cfg.SettingsFactory - Default batch fetch size: 1
550 [main] INFO org.hibernate.cfg.SettingsFactory - Generate SQL with comments: disabled
550 [main] INFO org.hibernate.cfg.SettingsFactory - Order SQL updates by primary key: disabled
550 [main] INFO org.hibernate.cfg.SettingsFactory - Order SQL inserts for batching: disabled
550 [main] INFO org.hibernate.cfg.SettingsFactory - Query translator: org.hibernate.hql.ast.ASTQueryTranslatorFactory
551 [main] INFO org.hibernate.hql.ast.ASTQueryTranslatorFactory - Using ASTQueryTranslatorFactory
551 [main] INFO org.hibernate.cfg.SettingsFactory - Query language substitutions: {}
551 [main] INFO org.hibernate.cfg.SettingsFactory - JPA-QL strict compliance: enabled
551 [main] INFO org.hibernate.cfg.SettingsFactory - Second-level cache: enabled
551 [main] INFO org.hibernate.cfg.SettingsFactory - Query cache: disabled
551 [main] INFO org.hibernate.cfg.SettingsFactory - Cache region factory : org.hibernate.cache.impl.NoCachingRegionFactory
552 [main] INFO org.hibernate.cfg.SettingsFactory - Optimize cache for minimal puts: disabled
552 [main] INFO org.hibernate.cfg.SettingsFactory - Structured second-level cache entries: disabled
554 [main] INFO org.hibernate.cfg.SettingsFactory - Echoing all SQL to stdout
555 [main] INFO org.hibernate.cfg.SettingsFactory - Statistics: disabled
555 [main] INFO org.hibernate.cfg.SettingsFactory - Deleted entity synthetic identifier rollback: disabled
555 [main] INFO org.hibernate.cfg.SettingsFactory - Default entity-mode: pojo
555 [main] INFO org.hibernate.cfg.SettingsFactory - Named query checking : enabled
555 [main] INFO org.hibernate.cfg.SettingsFactory - Check Nullability in Core (should be disabled when Bean Validation is on): enabled
568 [main] INFO org.hibernate.impl.SessionFactoryImpl - building session factory
668 [main] INFO org.hibernate.impl.SessionFactoryObjectFactory - Not binding factory to JNDI, no JNDI name configured
673 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - Running hbm2ddl schema export
673 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - exporting generated schema to database
679 [main] INFO org.hibernate.tool.hbm2ddl.SchemaExport - schema export complete
Hibernate: select max(id) from EVENTS
Hibernate: insert into EVENTS (EVENT_DATE, title, id) values (?, ?, ?)
Hibernate: insert into EVENTS (EVENT_DATE, title, id) values (?, ?, ?)
Hibernate: select event0_.id as id0_, event0_.EVENT_DATE as EVENT2_0_, event0_.title as title0_ from EVENTS event0_
Event (2014-01-31 00:43:44.237) : Our very first event!
Event (2014-01-31 00:43:44.259) : A follow up event
872 [main] INFO org.hibernate.impl.SessionFactoryImpl - closing
872 [main] INFO org.hibernate.connection.DriverManagerConnectionProvider - cleaning up connection pool: jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1;MVCC=TRUE
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.932 sec

Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.3.2:jar (default-jar) @ hibernate-tutorial-entitymanager ---
[WARNING] JAR will be empty - no content was marked for inclusion!
[INFO] Building jar: /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.0/src/hibernate-distribution-3.6.0.Final/project/documentation/quickstart/tutorials/entitymanager/target/hibernate-tutorial-entitymanager-3.6.0.Final.jar
[INFO] 
[INFO] --- maven-install-plugin:2.3.1:install (default-install) @ hibernate-tutorial-entitymanager ---
[INFO] Installing /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.0/src/hibernate-distribution-3.6.0.Final/project/documentation/quickstart/tutorials/entitymanager/target/hibernate-tutorial-entitymanager-3.6.0.Final.jar to /home/kamilos/.m2/repository/org/hibernate/tutorials/hibernate-tutorial-entitymanager/3.6.0.Final/hibernate-tutorial-entitymanager-3.6.0.Final.jar
[INFO] Installing /home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/hibernate/hibernate-3.6.0/src/hibernate-distribution-3.6.0.Final/project/documentation/quickstart/tutorials/entitymanager/pom.xml to /home/kamilos/.m2/repository/org/hibernate/tutorials/hibernate-tutorial-entitymanager/3.6.0.Final/hibernate-tutorial-entitymanager-3.6.0.Final.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 2.667s
[INFO] Finished at: Fri Jan 31 00:43:44 CET 2014
[INFO] Final Memory: 12M/150M
[INFO] ------------------------------------------------------------------------
