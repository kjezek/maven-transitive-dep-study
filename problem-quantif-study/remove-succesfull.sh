#/bin/bash

for f in enforcer-results/*.txt
do

  # remove files not having the enforcer error message
  if grep --quiet "Dependency convergence error for" $f; then
    echo $f
  else   
    rm $f
  fi  
  
done